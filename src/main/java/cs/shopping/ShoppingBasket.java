package cs.shopping;

import cs.shopping.entity.Offer;
import cs.shopping.entity.ShoppingItem;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This class holds all the items in the shopping basket.
 * It also provide interfaces to retrieve/add items and calculate total price for the basket.
 */
public class ShoppingBasket {
    private final PriceCatalogue catalogue;

    private List<String> items; //Items in basket with items names
    private Map<String, Long> itemCountMap;  //holds the count of each item in the basket

    /**
     * Default constructor.
     */
    public ShoppingBasket() {
        catalogue = PriceCatalogue.getInstance(true); //Load the price catalogue on initialisation.
        items = new ArrayList<>(); //Start with empty basket
        itemCountMap = new HashMap<>();
    }

    public boolean hasPriceCatelogue() {
        return null != catalogue;
    }

    public List<String> getItems() {
        return items;
    }

    /**
     * Load the basket with items and populate the item count map accordingly.
     * @param items List of items
     */
    public void setItems(List<String> items) {
        this.items = items;
        itemCountMap = items.stream().collect(
                Collectors.groupingBy(Function.identity(), Collectors.counting()
                )
        );
    }

    /**
     * Overloaded method with Array as input. Subsequently update the item count map.
     * @param items Array of items
     */
    public void setItems(String[] items) {
        setItems(new ArrayList(Arrays.asList(items)));
    }

    /**
     * Add single item to the basket. Update the item count map accordingly.
     * @param item
     */
    public void addItem(String item) {
        items.add(item);
        Long count = itemCountMap.get(item);
        if (count != null) {
            itemCountMap.put(item, count + 1);
        } else {
            itemCountMap.put(item, 1L);
        }
    }

    /**
     * Check eligibility of the item for the offer.
     *
     * @param item
     * @return Offer if available, otherwise null.
     */
    public Offer isItemEligibleForOffer(String item) {
        Offer itemOffer = catalogue.getItemOffer(item);
        Long itemCount = itemCountMap.get(item);
        if (itemOffer != null && itemCount != null && itemOffer.getMin() <= itemCount && itemOffer.getMax() >= itemCount) {
            return itemOffer;
        }
        return null;
    }

    /**
     * Get the total number of items in the basket.
     * @return total count of items
     */
    public long getTotalItemCount() {
        return itemCountMap.values().stream().collect(Collectors.summingLong(Long::longValue));
    }

    /**
     * Calculate the total prices of the items
     * Go through each item in the basket and add the cost to the total, the cost is calculated as: unitPrice*count
     * For the items that has offer on:
     *          - check if the item is eligible
     *          - Apply the discount offer for every item in the group of discount, the group is the maximum number of items that is divisible by the min value of the offer.
     *          - Apply the normal price for the remainder of the item on offer if the count is not divisible by the group count (min value of the offer)
     *          - Add the two cost together gives the total cost of the items on offer
     * @return total prices of items in basket.
     */
    public Double getTotalPrice() {
        Double total = 0D;
        for (Map.Entry<String, Long> entry : itemCountMap.entrySet()) {
            String item = entry.getKey();
            Long count = entry.getValue();
            ShoppingItem shoppingItem = catalogue.getItem(item);
            if (shoppingItem != null) {
                Double unitPrice = shoppingItem.getUnitPrice();
                Offer offer = isItemEligibleForOffer(item);
                if (offer != null) {
                    System.out.println("Applying offer for " + item+ ": " + offer.getDescription());
                    long remainder = count % offer.getMin(); // Reminder items with no discount applied
                    total += unitPrice * (1 - offer.getDiscount()) * (count - remainder) + unitPrice * remainder;
                } else {
                    total += unitPrice * count;
                }
            } else {
                //Ignore the item which we do not have price for
                System.out.println("No price information found for " + item);
            }
        }
        return total;
    }


    /**
     * Main method to calculate price
     * @param args
     */
    public static void main(String[] args) {
        ShoppingBasket sb = new ShoppingBasket();
        String[] items = {ItemName.Apple.toString(),ItemName.Melon.toString(), ItemName.Melon.toString()};
        sb.setItems(items);
        System.out.println("Total price for the shopping basket is: £" + sb.getTotalPrice().doubleValue());
    }

}
