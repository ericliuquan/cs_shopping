package cs.shopping.entity;

public class ShoppingItem extends Entity{
    private String name;
    private Double unitPrice; //Price are in pounds
    private Long offerId;  //Assuming an item can only have one offer at a time

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }
}
