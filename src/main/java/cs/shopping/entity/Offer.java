package cs.shopping.entity;

public class Offer extends Entity{
    //minimum quantity, this value is also used as the frequency of discount. e.g, the min for buy 1 get 1 free discount
    //is 2, therefore we apply the discount every 2 elements. The remainder will cost the normal price.
    private Integer min;
    //maximum quantity
    private Integer max;
    //discount factor. e.g. buy one get one free implies 0.5 discount factor, three for 2 will implies 2/3 discount
    private Double discount;
    private String description;

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
