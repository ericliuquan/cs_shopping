package cs.shopping;

/**
 * Enum to hold the item names as constants
 */
public enum ItemName {
    Apple,
    Banana,
    Melon,
    Lime,
    Strawberry
}
