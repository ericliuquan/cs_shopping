package cs.shopping.repository;

import cs.shopping.entity.Offer;

import java.util.Map;
import java.util.function.Function;

/**
 * Load offers from the offers.txt. This data should really be stored in a type bounded database.
 * Assuming the data are correctly populated in the file
 */
public class OfferRepository extends BaseFileRepository<Offer> {


    /**
     * Load all the records from the file and map them to Offer objects
     * @return all offers
     */
    @Override
    public Map<Long, Offer> loadEntries() {
        Function<String, Offer> mapToOffer = (line) -> {
            String[] p = line.split(",");
            Offer offer = new Offer();
            offer.setId(Long.parseLong(p[0]));
            if (isNotEmptyEntry(p,1))
                offer.setMin(Integer.parseInt(p[1]));
            if (isNotEmptyEntry(p,2))
                offer.setMax(Integer.parseInt(p[2]));
            if (isNotEmptyEntry(p,3))
                offer.setDiscount(Double.parseDouble(p[3]));
            if (isNotEmptyEntry(p,4))
                offer.setDescription(p[4]);
            return offer;
        };

        return loadItemsFromFile("offers.txt", mapToOffer);
    }
}
