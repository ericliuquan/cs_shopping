package cs.shopping.repository;

import cs.shopping.entity.ShoppingItem;

import java.util.Map;
import java.util.function.Function;

/**
 * Load shopping items from the items.txt. This data should really be stored in a type bounded database.
 * Assuming the data are correctly populated in the file
 */
public class ShoppingItemRepository extends BaseFileRepository<ShoppingItem> {

    /**
     * Load all the records from the file and map them to ShoppingItem objects
     * @return all shopping items
     */
    @Override
    public Map<Long, ShoppingItem> loadEntries() {
        Function<String, ShoppingItem> mapToShoppingItem = (line) -> {
            String[] p = line.split(",");
            ShoppingItem item = new ShoppingItem();
            item.setId(Long.parseLong(p[0]));
            item.setName(p[1]);
            item.setUnitPrice(Double.parseDouble(p[2]));
            //Offer reference is optional
            if (isNotEmptyEntry(p, 3)) {
                item.setOfferId(Long.parseLong(p[3]));
            }
            return item;
        };

        return loadItemsFromFile("items.txt", mapToShoppingItem);
    }
}
