package cs.shopping.repository;

import cs.shopping.entity.Entity;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Base class for file based data access
 * @param <T> Entity type
 */
public abstract class BaseFileRepository<T extends Entity> implements IRepository{

    /**
     * Check that the column value exist and is not empty
     * @param line record array
     * @param index column index
     * @return True/False
     */
    boolean isNotEmptyEntry(String[] line, int index) {
        return line.length>index && line[index]!=null && line[index].trim().length()>0;
    }


    /**
     *
     * @param fileName File to load data from
     * @param mapFct Function to map the line in the file to the entity object
     * @return Map with entity id as key and entity object as value
     */
    Map<Long, T> loadItemsFromFile(String fileName, Function<String, T> mapFct) {
        Map<Long, T> items = new HashMap<>();
        try {
            items = Files.lines(Paths.get(this.getClass().getClassLoader().getResource(fileName).toURI())).skip(1)
                    .map(mapFct).collect(Collectors.toMap(T::getId, Function.identity()));
        } catch (Exception e) {
            //Do not failing the app on parsing the file
            System.out.println("Unable to load file: " + fileName);
            e.printStackTrace();
        }
        return items;
    }

}
