package cs.shopping.repository;

import java.util.Map;

/**
 * Interface for database operations
 * @param <T>
 */
public interface IRepository<T> {
    /**
     * Load all records from database
     * @return Map with entity id as key and entity object as value
     */
    Map<Long, T> loadEntries();
}
