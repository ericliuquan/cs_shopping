package cs.shopping;

import cs.shopping.entity.Offer;
import cs.shopping.entity.ShoppingItem;
import cs.shopping.repository.IRepository;
import cs.shopping.repository.OfferRepository;
import cs.shopping.repository.ShoppingItemRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * This Singleton class holds all the shopping items and offer information available in the database.
 */
public class PriceCatalogue {
    private static PriceCatalogue instance;

    //Map with offer id as key and offer as value
    private Map<Long, Offer> offers;
    private List<ShoppingItem> items;

    private IRepository offerRepo;
    private IRepository shoppingRepo;

    /**
     * This should be only called by the internal getInstance() method.
     * All the items and offers are loaded via the corresponding repository object.
     */
    private PriceCatalogue(){
        offerRepo = new OfferRepository();
        shoppingRepo = new ShoppingItemRepository();
        offers = offerRepo.loadEntries();
        items = new ArrayList<>(shoppingRepo.loadEntries().values());
    }

    /**
     * Return the singleton instance of the class.
     * Create a new instance if the refresh flag is true.
     * @param refresh Flag to show if we need to reload the data, therefore a new instance
     * @return PriceCatalogue instance
     */
    public static PriceCatalogue getInstance(boolean refresh) {
        if (instance == null || refresh) {
            instance = new PriceCatalogue();
        }
        return instance;
    }

    public List<ShoppingItem> getItems() {
        return items;
    }

    public Map<Long, Offer> getOffers() {
        return offers;
    }

    /**
     * Retrieve the offer for the given item if available
     * @param itemName
     * @return offer if available, otherwise null
     */
    public Offer getItemOffer(String itemName) {
        Optional<ShoppingItem> item = items.stream().filter(x->x.getName().equals(itemName)).findFirst();
        if (item.isPresent() && item.get().getOfferId()!=null) {
            return offers.get(item.get().getOfferId());
        }
        return null;
    }

    /**
     * Retrieve the ShoppingItem object for the given item name if available
     * @param itemName
     * @return ShoppingItem if available, otherwise null.
     */
    public ShoppingItem getItem(String itemName) {
        Optional<ShoppingItem> item = items.stream().filter(x->x.getName().equals(itemName)).findFirst();
        return item.isPresent()?item.get() : null;
    }
}
