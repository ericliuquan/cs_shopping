package cs.shopping;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ShoppingBasketTest {
    private ShoppingBasket sb;

    @Before
    public void setUp() {
        sb = new ShoppingBasket();
    }

    @After
    public void tearDown() {
        sb = null;
    }

    @Test
    public void testShoppingBasketInitialisation() {
        assertTrue(sb.hasPriceCatelogue());
        assertNotNull(sb.getItems());
    }

    @Test
    public void testShoppingBasketItemCount() {
        assertEquals(0L, sb.getTotalItemCount());
        String[] items = {ItemName.Apple.toString(), ItemName.Banana.toString(), ItemName.Apple.toString() };
        sb.setItems(items);
        assertEquals(3L, sb.getTotalItemCount());
    }

    @Test
    public void testTotalPriceSingleItemWithoutOffer() {
        String[] items = {ItemName.Banana.toString()};
        sb.setItems(items);
        assertEquals(new Double(0.2), sb.getTotalPrice());
    }

    @Test
    public void testTotalPriceMultipleItemsWithoutOffer() {
        String[] items = {ItemName.Apple.toString(), ItemName.Apple.toString()};
        sb.setItems(items);
        assertEquals(new Double(0.7), sb.getTotalPrice());
    }

    @Test
    public void testTotalPriceSingleItemWithOffer() {
        String[] items = {ItemName.Melon.toString(), ItemName.Melon.toString()};
        sb.setItems(items);
        assertEquals(new Double(0.5), sb.getTotalPrice());
        sb.addItem(ItemName.Melon.toString());
        sb.addItem(ItemName.Melon.toString());
        assertEquals(new Double(1), sb.getTotalPrice());
    }

    @Test
    public void testShoppingBasketSingleItemWithOfferWithRemainder() {
        String[] items = {ItemName.Melon.toString(), ItemName.Melon.toString(), ItemName.Melon.toString(),};
        sb.setItems(items);
        assertEquals(new Double(1.0), sb.getTotalPrice());
    }


    @Test
    public void testShoppingBasketSingleItemWithOfferBelowMax() {
        String[] items = {ItemName.Melon.toString()};
        sb.setItems(items);
        assertEquals(new Double(0.5), sb.getTotalPrice());
    }

    @Test
    public void testShoppingBasketSingleItemWithOfferExceedMax() {
        for (int i=0; i<10;i++)
            sb.addItem(ItemName.Melon.toString());
        assertEquals(new Double(5.0), sb.getTotalPrice());
    }

    @Test
    public void testShoppingBasketMultipleItemsWithOffer() {
        String[] items = {ItemName.Apple.toString(),ItemName.Melon.toString(), ItemName.Melon.toString()};
        sb.setItems(items);
        assertEquals(new Double(0.85), sb.getTotalPrice());
    }

    @Test
    public void testShoppingBasketItemWithNoPrice() {
        String[] items = {ItemName.Apple.toString(),ItemName.Strawberry.toString()};
        sb.setItems(items);
        assertEquals(new Double(0.35), sb.getTotalPrice());
    }

    @Test
    public void testShoppingBasketInvalid() {
        String[] items = {"Blah"};
        sb.setItems(items);
        assertEquals(new Double(0), sb.getTotalPrice());
    }

    @Test
    public void testIsItemEligibleForOfferNegative() {
        String[] items = {ItemName.Apple.toString(), ItemName.Banana.toString(), ItemName.Apple.toString() };
        sb.setItems(items);
        assertNull(sb.isItemEligibleForOffer(ItemName.Apple.toString()));
    }


    @Test
    public void testIsItemEligibleForOfferPositive() {
        String[] items = {ItemName.Apple.toString(), ItemName.Melon.toString(), ItemName.Melon.toString() };
        sb.setItems(items);
        assertNotNull(sb.isItemEligibleForOffer(ItemName.Melon.toString()));
    }

    @Test
    public void testIsItemEligibleForOfferExceedMax() {
        for (int i=0; i<10;i++)
            sb.addItem(ItemName.Melon.toString());
        assertNull(sb.isItemEligibleForOffer(ItemName.Melon.toString()));
    }

    @Test
    public void testIsItemEligibleForAddItem() {
        String[] items = {ItemName.Apple.toString(), ItemName.Banana.toString(), ItemName.Melon.toString() };
        sb.setItems(items);
        assertNull(sb.isItemEligibleForOffer(ItemName.Melon.toString()));
        sb.addItem(ItemName.Melon.toString());
        assertNotNull(sb.isItemEligibleForOffer(ItemName.Melon.toString()));
    }

}
