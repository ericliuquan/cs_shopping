package cs.shopping.repository;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class BaseRepositoryTest {

    private BaseFileRepository repo;

    @Before
    public void setUp(){
        repo = new BaseFileRepository() {
            @Override
            public Map loadEntries() {
                return null;
            }
        };
    }

    @Test
    public void testIsNotEmptyEntry() {
        String[] test = {"a", "b", "c"};
        assertTrue(repo.isNotEmptyEntry(test, 0));
        assertTrue(repo.isNotEmptyEntry(test, 1));
        assertTrue(repo.isNotEmptyEntry(test, 2));
        assertFalse(repo.isNotEmptyEntry(test, 3));
    }

    @Test
    public void testLoadFromInvalidFile() {
        repo.loadItemsFromFile("randomFile.txt", null);
    }
}
