package cs.shopping;

import static org.junit.Assert.*;

import cs.shopping.entity.Offer;
import org.junit.Before;
import org.junit.Test;

public class PriceCatalogueTest {

    private PriceCatalogue catalogue;

    @Before
    public void setup() {
        catalogue = PriceCatalogue.getInstance(true);
    }

    @Test
    public void testPriceCatalogueInitialisation() {
        assertFalse(catalogue.getOffers().isEmpty());
        assertEquals(2, catalogue.getOffers().size());
        assertFalse(catalogue.getItems().isEmpty());
    }

    @Test
    public void testGetItemOffer() {
        assertNull(catalogue.getItemOffer(ItemName.Apple.toString()));
        assertNull(catalogue.getItemOffer(ItemName.Banana.toString()));

        Offer melonOffer = catalogue.getItemOffer(ItemName.Melon.toString());
        assertNotNull(melonOffer);
        assertEquals(new Integer(2), melonOffer.getMin());
        assertEquals(new Integer(8), melonOffer.getMax());
        Offer limeOffer = catalogue.getItemOffer(ItemName.Lime.toString());
        assertEquals(new Integer(3), limeOffer.getMin());
        assertNull(limeOffer.getMax());
        assertEquals(new Double(0.67), limeOffer.getDiscount());
    }

    @Test
    public void testGetItem() {
        assertNotNull(catalogue.getItem(ItemName.Apple.toString()));
        assertNotNull(catalogue.getItem(ItemName.Banana.toString()));
        assertNotNull(catalogue.getItem(ItemName.Melon.toString()));
        assertNotNull(catalogue.getItem(ItemName.Lime.toString()));
        assertNull(catalogue.getItem(ItemName.Strawberry.toString())); //Item does not exist
    }

    @Test
    public void testGetItemPrice() {
        assertEquals(new Double(0.35), catalogue.getItem(ItemName.Apple.toString()).getUnitPrice());
        assertEquals(new Double(0.20), catalogue.getItem(ItemName.Banana.toString()).getUnitPrice());
        assertEquals(new Double(0.50), catalogue.getItem(ItemName.Melon.toString()).getUnitPrice());
        assertEquals(new Double(0.15), catalogue.getItem(ItemName.Lime.toString()).getUnitPrice());
    }
}
